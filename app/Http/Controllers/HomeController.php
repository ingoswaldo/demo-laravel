<?php

namespace App\Http\Controllers;

use App\Charts\HighChart;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function graficar()
    {
        $cantidadArray = rand(1,10);
        $datosArray = [];
        $labelsArray = [];

        for ($i = 1; $i <= $cantidadArray; $i++){
            $datosArray[] = rand(1,100);
            $labelsArray[] = 'Tiempo '. $i;
        }

        $highChart = new HighChart();
        $highChart->labels($labelsArray);
        $highChart->label('Valor');
        $highChart->dataset('Tiempos', 'line', $datosArray);

        $datos['highChart'] = $highChart;

        return view('graficar', $datos);
    }

    public function index()
    {
        return view('home');
    }
}
