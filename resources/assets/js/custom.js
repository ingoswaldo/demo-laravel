;(function (global, $) {
    "use strict";

    //variable global
    let ADMIN = global.ADMIN = global.ADMIN || {};
    let spin = new Spinner();

    //elementos globales
    ADMIN.ELEMENTS = {};
    ADMIN.ELEMENTS.body = $('body');

    //eventos
    ADMIN.ELEMENTS.body.on('submit', '.guardar-ajax', guardarAjax);

    //funciones
    function guardarAjax(evento) {
        evento.preventDefault();

        let form = $(evento.currentTarget);

        let options = {
            url: form.attr('action'), type: form.attr('method'), dataType: 'json',
            data: form.serialize(),
            beforeSend: function () {
                spinner(true);
            },
            success: function (respuesta) {
                spinner(false);
                quitarErrores();
                form.trigger('reset');
                window.location.href = respuesta.url;
            },
            error: function (respuesta) {
                spinner(false);
                switch (respuesta.status) {
                    case 422:
                        quitarErrores();
                        mostrarErrores(respuesta);
                        break;

                }
            }
        };

        $.ajax(options);
    }

    function spinner(show) {
        if (show){
            spin.spin($('form')[0]);
        } else{
            spin.stop();
        }
    }

    function quitarErrores() {
        $('input').removeClass('is-invalid');
        $('strong', 'span[data-message]').text('');
    }

    function mostrarErrores(respuesta) {
        let errores = JSON.parse(respuesta.responseText).errors;

        $.each(errores, function (elemento, indice) {
            $('input[name="'+elemento+'"]').addClass('is-invalid');
            $('strong', 'span[data-message="' + elemento + '"]').text(indice);
        });
    }

})(window, jQuery);