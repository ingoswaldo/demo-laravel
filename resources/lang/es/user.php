<?php

return [
    'email' => 'Correo Electrónico',
    'password' => 'Contraseña',
    'password_confirm' => 'Confirmar Contraseña',
    'registrar' => 'Registrar Usuario',
    'cerrar_sesion' => 'Cerrar Sesión',
    'login' => 'Iniciar Sesión',
    'name' => 'Nombre',
    'grafica' => 'Grafica',
    'inicio' => 'Inicio',
    'mensaje_inicio' => 'Has entrado al sistema'
];