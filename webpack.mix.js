let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/css/spin.css',
    ], 'public/css/app.css').version();

mix.combine([
    'resources/assets/js/jquery-3.3.1.min.js',
    'resources/assets/js/popper.min.js',
    'resources/assets/js/bootstrap.min.js',
    'resources/assets/js/spin.js',
    'resources/assets/js/custom.js',
    'resources/assets/js/highcharts.js',
], 'public/js/app.js').version();
